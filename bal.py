#! ../env/bin/python
# encoding: utf-8
import os.path
import struct
import time

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadHoldingRegistersRequest as read
from pymodbus.register_write_message import WriteMultipleRegistersRequest as write

from pymodbus.pdu import ExceptionResponse
from pymodbus.exceptions import ModbusException

import logging
log = logging.getLogger('BAL').addHandler(logging.NullHandler())

#функция для преобразования десятичного числа в 16ричное представление ввиде строки
shex = lambda num: '0x{:04X}'.format(num)


class InvalidRequest(ValueError):
    pass

class ConverterNotConnected(Exception):
    pass

class Client(object):
    def __init__(self):
        self.port = None
        self.fast_client = None
        self.midl_client = None
        self.slow_client = None

    def close(self):
        if self.fast_client:
            self.fast_client.close()
            self.fast_client = None
        if self.midl_client:
            self.midl_client.close()
            self.midl_client = None
        if self.slow_client:
            self.slow_client.close()
            self.slow_client = None


    def connect(self):
        for adapter_path in ['/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3','/dev/ttyUSB4']:
            if os.path.exists(adapter_path):
                break
        else:
            raise ConverterNotConnected("USB converter isn't connected.")

        self.port = adapter_path

        # for ST: 
        #   short reads up to 15 registers (~0.015s)
        #   long reads up to 120 registers (~0.060s)
        #   short writes up to 30 registers (~0.018s)
        self.fast_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=0.200)

        # for ST: 
        #   writes up to 100 registers (~0.045s)
        # for TM: 
        #   writes up to 40 registers (~0.022s)
        self.midl_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=0.500)
        ## for:
        ##   long writes up to 110 registers (~0.100s)
        self.slow_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=1)

        #return self.fast_client, self.midl_client, self.slow_client
        return self.fast_client

    def _client_for(self, req):
        return self.fast_client

        if isinstance(req, write):
            if req.count < 0:
                raise ValueError('req.count is < 0?? WAT!?')
            if 0 < req.count <= 10:
                return self.fast_client
            elif 10 < req.count <= 40:
                return self.midl_client
            elif 40 < req.count < 110:
                return self.slow_client
            else:
                raise InvalidRequest("Can't write more than 110 registers per request")

        elif isinstance(req, read):
            if req.count < 0:
                raise ValueError('req.count is < 0?? WAT!?')
            if 0 < req.count <= 125:
                return self.fast_client
            else:
                raise InvalidRequest("Can't read more than 120 registers per request")

    def execute(self, req):
        time.sleep(0.01) #таймаут между послыками ~5 символов
        try:
            resp = self._client_for(req).execute(req)
        except ModbusException as e:
            print e
            return None

        if isinstance(resp, ExceptionResponse):
            print resp
            return None
        else:
            return resp


if __name__ == '__main__':
    pass
