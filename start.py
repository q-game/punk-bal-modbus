#!../env/bin/python
# encoding: utf-8
import json
import random
import socket
#host = socket.gethostbyname
def host(*args):
    return '127.0.0.1'

import zmq
from bal import Client, read, write
import logging
import os
import signal
import sys

ADRS = {
    'VBAL': {
        'ip': host('qg_bal_modbus'),
        'port': '25000'
    },
    'LOGS': {
        'ip': host('qg_logger'),
        'port': '25001'
    }
}
zmq_ctx = zmq.Context.instance()


#statndalone util lib for project logging
class ModifyFilter(logging.Filter):
    def __init__(self, *args, **kwargs):
        super(ModifyFilter, self).__init__()
        self.prefix = kwargs.get('prefix', '[   ]')
        self.name = kwargs.get('name', None)

    def filter(self, record):
        record.prefix = self.prefix
        record.short_levelname = {
            logging.DEBUG: 'DEBG',
            logging.INFO: 'INFO',
            logging.WARNING: 'WARN',
            logging.ERROR: 'ERRR',
            logging.CRITICAL: 'CRIT'
        }.get(record.levelno, '????')
        if self.name:
            record.name = self.name

        return True

class PUBHandler(logging.Handler):
    def __init__(self, sock):
        super(PUBHandler, self).__init__()
        self.sock = sock
        self.sock.send('PING!')

    def emit(self, record):
        msg = self.format(record)
        self.sock.send(msg)


log = logging.getLogger('BAL')
log.setLevel(logging.ERROR)
log.addFilter(ModifyFilter())

#logger_sock = zmq_ctx.socket(zmq.PUB)
#logger_sock.connect('tcp://{ip}:{port}'.format(**ADRS['LOGS']))
#handler = PUBHandler(logger_sock)
formatter = logging.Formatter("%(asctime)s|%(name)s%(prefix)s|%(short_levelname)s|%(message)s")
#handler.setFormatter(formatter)

#log.addHandler(handler)

console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
log.addHandler(console_handler)

pmb_log = logging.getLogger('pymodbus')
pmb_log.setLevel(logging.ERROR)
pmb_log.addHandler(logging.StreamHandler())

STATUS = {
    'stop': False
}

import os.path
import time
from bal import ConverterNotConnected
from serial import SerialException

def start_bal(handler):
    client = Client()
    while True:
        try:
            client.connect()
        except ConverterNotConnected as e:
            print e
            time.sleep(1)
        else:
            break
    try:
        address = "tcp://{ip}:{port}".format(**ADRS['VBAL'])
        sock = zmq_ctx.socket(zmq.REP)
        sock.setsockopt(zmq.LINGER, 1000)
        sock.bind(address)

        def shutdown(signal, frame):
            print 'SIGTERM'
            sock.close()
            log.info("Termination of zmq context...")
            zmq_ctx.destroy()
            sys.exit(0)
        signal.signal(signal.SIGTERM, shutdown)

        poller = zmq.Poller()
        poller.register(sock, zmq.POLLIN)
        log.info('BAL started!')
        while not STATUS['stop']:
            connect = poller.poll(1000)
            if connect:
                request = json.loads(sock.recv())
                try:
                    #log.debug("RespServer: New request! {0}".format(request))
                    if not os.path.exists(client.port):
                        client.close()
                        while True:
                            try:
                                client.connect()
                            except ConverterNotConnected as e:
                                print e
                                time.sleep(1)
                            else:
                                break
                    while True:
                        try:
                            resp = handler(client, request)
                        except SerialException as e:
                            print e
                            time.sleep(1)
                        except OSError as e:
                            if e.errno == 13: # permission denied
                                print e
                                time.sleep(1)
                            else:
                                raise
                        else:
                            break
                except Exception as e:
                    resp = {"status": "fail", "info": "Exception in handler"}
                    log.debug("Exception in handler: {0}".format(e))
                    #raise e

                # Achtung: only for debug!!!
                #resp = {"status": "success", "info": "Debug mode"}
                sock.send(json.dumps(resp))
                #log.debug("RespServer: Send response! {0}".format(resp))
            else:
                pass
    except (KeyboardInterrupt, SystemExit):
        log.info("System interrupt...")

    finally:
        sock.close()
        log.info("Termination of zmq context...")
        zmq_ctx.destroy()

#to_hex = lambda s: int(s, 16)
def to_hex(s):
    if isinstance(s, int):
        #import pudb; pu.db
        return s
    try:
        return int(s, 16)
    except Exception as e:
        #import pudb; pu.db
        raise e

def handler(client, request):
    if request.get('type') == 'system':
        if request.get('cmd') == 'shutdown':
            STATUS['stop'] = True
            return

    elif request.get('type') == 'cmd':
        if request.get('cmd') == 'read':
            req = read(
                to_hex(request.get('register_addr')),
                int(request.get('registers_number')),
                unit=to_hex(request.get('device_addr'))
            )
            resp = client.execute(req)
            #log.info("Device response: {0}".format(resp))
            if resp:
                return {
                    "status": "success",
                    "data": list(resp.registers)
                }
            else:
                return {
                    "status": "fail",
                    "info": 'Cant read registers'
                }
        elif request.get('cmd') == 'write':
            #import pudb; pu.db
            req = write(
                to_hex(request.get('register_addr')),
                [to_hex(el) for el in request.get('data')],
                unit=to_hex(request.get('device_addr'))
            )
            resp = client.execute(req)
            #log.info("Device response: {0}".format(resp))
            if resp:
                return {
                    "status": "success",
                }
            else:
                return {
                    "status": "fail",
                    "info": 'Cant write registers'
                }
    else:
        return {
            "status": "fail",
            "info": "No such command: %s" % request.get('cmd')
        }


if __name__ == '__main__':
    start_bal(handler)
