#!/bin/bash

# Разворачиваетнеобходимую файловую структуру

cd `dirname "$0"`;
BRANCH_ROOT="../../";
USER_NAME=$USER;

#sudo usermod -a -G dialout {{username}}

if [ "$1" ]; then
    echo "Creating virtual python environment...";
    ./env_update.sh "$1" \
        && echo "  ok" \
        || echo "  ERROR!" 1>&2;
else
   echo "Error: python version requires as first argument" 1>&2;
fi
